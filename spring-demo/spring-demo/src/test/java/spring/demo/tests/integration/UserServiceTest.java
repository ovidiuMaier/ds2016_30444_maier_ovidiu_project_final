package spring.demo.tests.integration;


import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import spring.demo.errorhandler.EntityValidationException;
import spring.demo.errorhandler.ResourceNotFoundException;
import spring.demo.dto.UserDTO;
import spring.demo.services.UserService;
import spring.demo.tests.config.TestJPAConfiguration;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { TestJPAConfiguration.class })
@Transactional
public class UserServiceTest {

	@Autowired
	private UserService userService;

	@Test
	public void testCreate() {

        UserDTO dto1 = new UserDTO.Builder()
                .email("user@gmail.com")
                .username("username")
                .password("password")
                .role("manager")
                .create();
        
        UserDTO dto2 = new UserDTO.Builder()
                .email("user@gmail.com")
                .username("username")
                .password("password")
                .role("client")
                .create();

		userService.create(dto1);
		userService.create(dto2);

		List<UserDTO> fromDB = userService.findAll();

		assertTrue("One entity inserted", fromDB.size() == 2);
	}
	
	@Test
	public void testDelete() {

        UserDTO dto1 = new UserDTO.Builder()
                .email("user@gmail.com")
                .username("username")
                .password("password")
                .role("manager")
                .create();
        
        UserDTO dto2 = new UserDTO.Builder()
                .email("user@gmail.com")
                .username("username")
                .password("password")
                .role("client")
                .create();

        int userId = userService.create(dto1);
		userService.create(dto2);

		List<UserDTO> fromDB = userService.findAll();

		assertTrue("Inserted", fromDB.size() == 2);
		userService.deleteUser(userId);
		
		fromDB = userService.findAll();
		assertTrue("Deleted", fromDB.size() == 1);
	}
	
	@Test(expected = ResourceNotFoundException.class)
	public void testDeleteUnsuccessful() {
		userService.deleteUser(-1);
	}
	
	@Test
	public void testUpdate() {
        UserDTO dto = new UserDTO.Builder()
                .email("user@gmail.com")
                .username("username")
                .password("password")
                .role("member")
                .create();
        
        int userId = userService.create(dto);
        UserDTO fromDB = userService.findUserById(userId);
        assertTrue("Role", "member".equals(fromDB.getRole()));
		
        UserDTO updatedto = new UserDTO.Builder()
                .role("client")
                .create();
        userService.updateUser(userId, updatedto);
        fromDB = userService.findUserById(userId);
        assertTrue("Role", "client".equals(fromDB.getRole()));
	}

	@Test
	public void testGetByIdSuccessful() {

        UserDTO dto = new UserDTO.Builder()
                .email("user@gmail.com")
                .username("username")
                .password("password")
                .role("manager")
                .create();

		int userId = userService.create(dto);
		UserDTO fromDB = userService.findUserById(userId);

		assertTrue("user@gmail.com", dto.getEmail().equals(fromDB.getEmail()));
		assertTrue("password", dto.getPassword().equals(fromDB.getPassword()));
		assertTrue("manager", dto.getRole().equals(fromDB.getRole()));
	}

	@Test(expected = EntityValidationException.class)
	public void testCreateUnsuccessfulEmail() {

		UserDTO dto = new UserDTO.Builder()
				.password("password")
				.role("manager")
				.create();

		int userId = userService.create(dto);
		UserDTO fromDB = userService.findUserById(userId);

		assertTrue("password", dto.getPassword().equals(fromDB.getPassword()));
		assertTrue("manager", dto.getRole().equals(fromDB.getRole()));
	}

	@Test(expected = ResourceNotFoundException.class)
	public void testGetByIdUnsuccessful() {

        UserDTO dto = new UserDTO.Builder()
        		.email("user@gmail.com")
        		.username("username")
                .password("password")
                .role("manager")
                .create();

		int userId = userService.create(dto);
		userService.findUserById(userId + 1);

	}

}
