package spring.demo.services;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import spring.demo.errorhandler.EntityValidationException;
import spring.demo.errorhandler.ResourceNotFoundException;
import spring.demo.dto.UserDTO;
import spring.demo.entities.User;
import spring.demo.repositories.UserRepository;

@Service
public class UserService {
	private static final String SPLIT_CH = " ";
	public static final Pattern VALID_EMAIL_ADDRESS_REGEX = Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$",
			Pattern.CASE_INSENSITIVE);

	@Autowired
	private UserRepository usrRepository;

	public UserDTO findUserById(int userId) {
		User usr = usrRepository.findById(userId);
		if (usr == null) {
			throw new ResourceNotFoundException(User.class.getSimpleName());
		}
		
		UserDTO dto = new UserDTO.Builder()
						.id(usr.getId())
						.email(usr.getEmail())
						.username(usr.getUsername())
						.role(usr.getRole())
						.password(usr.getPassword())
						.tasks(usr.getTasks())
						.create();
		return dto;
	}
	
	
	public UserDTO findUserByUsername(String username) {
		User usr = usrRepository.findByUsername(username);
		if (usr == null) {
			throw new ResourceNotFoundException(User.class.getSimpleName());
		}
		
		UserDTO dto = new UserDTO.Builder()
						.id(usr.getId())
						.email(usr.getEmail())
						.username(usr.getUsername())
						.role(usr.getRole())
						.password(usr.getPassword())
						.tasks(usr.getTasks())
						.create();
		return dto;
	}
	
	
	public UserDTO findUserByEmail(String email) {
		List<User> users = usrRepository.findAll();
		System.out.println("aici" + email);
		UserDTO usr = null;
		for (User user : users) {
			if(user.getEmail().equals(email)){
				System.out.println(user.getPassword());
			usr = new UserDTO.Builder()
				.id(user.getId())
				.email(user.getEmail())
				.username(user.getUsername())
				.role(user.getRole())
				.password(user.getPassword())
				.tasks(user.getTasks())
				.create();
			}
	
		}
//		System.out.println(usr.getPassword());
		return usr;
	}
	
	
	public UserDTO deleteUser(int userId) {
		User usr = usrRepository.findById(userId);
		if (usr == null) {
			throw new ResourceNotFoundException(User.class.getSimpleName());
		}
		usrRepository.delete(userId);
		
		UserDTO dto = new UserDTO.Builder()
				.id(usr.getId())
				.email(usr.getEmail())
				.username(usr.getUsername())
				.role(usr.getRole())
				.password(usr.getPassword())
				.create();
		return dto;
	}
	
	public int updateUser(int userId, UserDTO userDTO) {
		
		User user = usrRepository.findById(userId);
		if (user == null) {
			throw new ResourceNotFoundException(User.class.getSimpleName());
		}
		
		if (userDTO.getEmail() != null) user.setEmail(userDTO.getEmail());
		if (userDTO.getUsername() != null) user.setUsername(userDTO.getUsername());
		if (userDTO.getPassword() != null) user.setPassword(userDTO.getPassword());
		if (userDTO.getRole() != null) user.setRole(userDTO.getRole());
		user.setCreated(new Date());
		
		User usr = usrRepository.save(user);
		return usr.getId();
	}
	
	public List<UserDTO> findAll() {
		List<User> users = usrRepository.findAll();
		List<UserDTO> toReturn = new ArrayList<UserDTO>();
		for (User user : users) {
			UserDTO dto = new UserDTO.Builder()
						.id(user.getId())
						.email(user.getEmail())
						.username(user.getUsername())
						.role(user.getRole())
						.password(user.getPassword())
						.create();
			toReturn.add(dto);
		}
		return toReturn;
	}
	
	public List<UserDTO> findAllClients() {
		List<User> users = usrRepository.findAll();
		List<UserDTO> toReturn = new ArrayList<UserDTO>();
		for (User user : users) {
			if (user.getRole().equals("client")){
				UserDTO dto = new UserDTO.Builder()
							.id(user.getId())
							.email(user.getEmail())
							.username(user.getUsername())
							.role(user.getRole())
							.password(user.getPassword())
							.create();
				toReturn.add(dto);
			}
		}
		return toReturn;
	}
	
	public List<UserDTO> findAllMembers() {
		List<User> users = usrRepository.findAll();
		List<UserDTO> toReturn = new ArrayList<UserDTO>();
		for (User user : users) {
			if (user.getRole().equals("member")){
				UserDTO dto = new UserDTO.Builder()
							.id(user.getId())
							.email(user.getEmail())
							.username(user.getUsername())
							.role(user.getRole())
							.password(user.getPassword())
							.create();
				toReturn.add(dto);
			}
		}
		return toReturn;
	}
	

	public int create(UserDTO userDTO) {
		List<String> validationErrors = validateUser(userDTO);
		if (!validationErrors.isEmpty()) {
			throw new EntityValidationException(User.class.getSimpleName(),validationErrors);
		}

		User user = new User();
		user.setEmail(userDTO.getEmail());
		user.setUsername(userDTO.getUsername());
		user.setPassword(userDTO.getPassword());
		user.setRole(userDTO.getRole());
		user.setCreated(new Date());
		
		User usr = usrRepository.save(user);
		return usr.getId();
	}

	private List<String> validateUser(UserDTO usr) {
		List<String> validationErrors = new ArrayList<String>();

		if (usr.getEmail() == null || !validateEmail(usr.getEmail())) {
			validationErrors.add("Email does not have the correct format.");
		}

		return validationErrors;
	}

	public static boolean validateEmail(String email) {
		Matcher matcher = VALID_EMAIL_ADDRESS_REGEX.matcher(email);
		return matcher.find();
	}
}
