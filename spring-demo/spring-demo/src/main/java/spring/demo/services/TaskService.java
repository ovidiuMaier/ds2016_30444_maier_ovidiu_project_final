package spring.demo.services;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import spring.demo.errorhandler.EntityValidationException;
import spring.demo.errorhandler.ResourceNotFoundException;
import spring.demo.dto.*;
import spring.demo.entities.Task;
import spring.demo.entities.User;
import spring.demo.repositories.TaskRepository;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.MailParseException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;

@Service
public class TaskService {
	private static final String SPLIT_CH = " ";
	
	@Autowired
	private TaskRepository tskRepository;

	public TaskDTO findTaskById(int taskId) {
		Task tsk = tskRepository.findById(taskId);
		if (tsk == null) {
			throw new ResourceNotFoundException(Task.class.getSimpleName());
		}
		
		TaskDTO dto = new TaskDTO.Builder()
						.id(tsk.getId())
						.title(tsk.getTitle())
						.status(tsk.getStatus())
						.description(tsk.getDescription())
						.users(tsk.getUsers())
						.create();
		return dto;
	}
	
	public TaskDTO deleteTask(int taskId) {
		Task tsk = tskRepository.findById(taskId);
		if (tsk == null) {
			throw new ResourceNotFoundException(Task.class.getSimpleName());
		}
		tskRepository.delete(taskId);
		
		TaskDTO dto = new TaskDTO.Builder()
				.id(tsk.getId())
				.title(tsk.getTitle())
				.status(tsk.getStatus())
				.description(tsk.getDescription())
				.create();
		return dto;
	}
	
	public int updateTask(int taskId, TaskDTO taskDTO) {
		
		Task task = tskRepository.findById(taskId);
		if (task == null) {
			throw new ResourceNotFoundException(Task.class.getSimpleName());
		}
		
		if (taskDTO.getTitle() != null) task.setTitle(taskDTO.getTitle());
		if (taskDTO.getDescription() != null) task.setDescription(taskDTO.getDescription());
		if (taskDTO.getStatus() != null) task.setStatus(taskDTO.getStatus());
		
		System.out.println(taskDTO.getStatus());
		
		if (taskDTO.getStatus().equals("completed")){
			System.out.println("here");
			JavaMailSenderImpl mailer = new JavaMailSenderImpl();
			mailer.setHost("smtp.gmail.com");
			mailer.setPort(587);
			mailer.setUsername("dsprojectt");
			mailer.setPassword("DS@distributed16");
			Properties props = new Properties();
			props.setProperty("mail.smtp.auth", "true");
			props.setProperty("mail.smtp.starttls.enable", "true");
			mailer.setJavaMailProperties(props);
			
			
			Set<User> users = task.getUsers();
			
			System.out.println(Integer.toString(users.size()));
			for(User u : users)
				if(u.getRole().equals("client"))
				{
					MimeMessage message = mailer.createMimeMessage();	   
						MimeMessageHelper helper;
						try {
							helper = new MimeMessageHelper(message, true);
							helper.setFrom("devtesting234@gmail.com");
							helper.setTo(u.getEmail());
							helper.setSubject("Task completed");
							helper.setText("The task " + task.getTitle() + " is completed");
							
						} catch (MessagingException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						//FileSystemResource attc = new FileSystemResource(file);
						//helper.addAttachment(attc.getFilename(), attc);
					     mailer.send(message);
				}
		}
		
		Task tsk = tskRepository.save(task);
		return tsk.getId();
	}
	
	public List<TaskDTO> findAll() {
		List<Task> tasks = tskRepository.findAll();
		List<TaskDTO> toReturn = new ArrayList<TaskDTO>();
		for (Task task : tasks) {
			TaskDTO dto = new TaskDTO.Builder()
						.id(task.getId())
						.title(task.getTitle())
						.status(task.getStatus())
						.description(task.getDescription())
						.create();
			toReturn.add(dto);
		}
		return toReturn;
	}

	public int create(TaskDTO taskDTO) {
		Task task = new Task();
		task.setTitle(taskDTO.getTitle());
		task.setDescription(taskDTO.getDescription());
		task.setStatus(taskDTO.getStatus());
		task.setUsers(taskDTO.getUsers());
		
		Task tsk = tskRepository.save(task);
		return tsk.getId();
	}

}
