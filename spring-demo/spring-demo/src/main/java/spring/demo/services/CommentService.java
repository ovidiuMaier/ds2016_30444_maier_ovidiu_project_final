package spring.demo.services;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import spring.demo.errorhandler.EntityValidationException;
import spring.demo.errorhandler.ResourceNotFoundException;
import spring.demo.dto.*;
import spring.demo.entities.Comment;
import spring.demo.entities.User;
import spring.demo.repositories.CommentRepository;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.MailParseException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;

@Service
public class CommentService {
	private static final String SPLIT_CH = " ";
	
	@Autowired
	private CommentRepository cmtRepository;

	public CommentDTO findCommentById(int commentId) {
		Comment cmt = cmtRepository.findById(commentId);
		if (cmt == null) {
			throw new ResourceNotFoundException(Comment.class.getSimpleName());
		}
		
		CommentDTO dto = new CommentDTO.Builder()
						.id(cmt.getId())
						.title(cmt.getTitle())
						.content(cmt.getContent())
						.task(cmt.getTask())
						.create();
		return dto;
	}
	
	public CommentDTO deleteComment(int commentId) {
		Comment cmt = cmtRepository.findById(commentId);
		if (cmt == null) {
			throw new ResourceNotFoundException(Comment.class.getSimpleName());
		}
		cmtRepository.delete(commentId);
		
		CommentDTO dto = new CommentDTO.Builder()
				.id(cmt.getId())
				.title(cmt.getTitle())
				.content(cmt.getContent())
				.create();
		return dto;
	}
	
//	public int updateComment(int commentId, CommentDTO commentDTO) {
//		
//		Comment comment = cmtRepository.findById(commentId);
//		if (comment == null) {
//			throw new ResourceNotFoundException(Comment.class.getSimpleName());
//		}
//		
//		if (commentDTO.getTitle() != null) comment.setTitle(commentDTO.getTitle());
//		if (commentDTO.getContent() != null) comment.setContent(commentDTO.getContent());
//		if (commentDTO.getStatus() != null) comment.setStatus(commentDTO.getStatus());
//		
//		System.out.println(commentDTO.getStatus());
//		
//		if (commentDTO.getStatus().equals("completed")){
//			System.out.println("here");
//			JavaMailSenderImpl mailer = new JavaMailSenderImpl();
//			mailer.setHost("smtp.gmail.com");
//			mailer.setPort(587);
//			mailer.setUsername("dsprojectt");
//			mailer.setPassword("DS@distributed16");
//			Properties props = new Properties();
//			props.setProperty("mail.smtp.auth", "true");
//			props.setProperty("mail.smtp.starttls.enable", "true");
//			mailer.setJavaMailProperties(props);
//			
//			
//			Set<User> users = comment.getUsers();
//			
//			System.out.println(Integer.toString(users.size()));
//			for(User u : users)
//				if(u.getRole().equals("client"))
//				{
//					MimeMessage message = mailer.createMimeMessage();	   
//						MimeMessageHelper helper;
//						try {
//							helper = new MimeMessageHelper(message, true);
//							helper.setFrom("devtesting234@gmail.com");
//							helper.setTo(u.getEmail());
//							helper.setSubject("Comment completed");
//							helper.setText("The comment " + comment.getTitle() + " is completed");
//							
//						} catch (MessagingException e) {
//							// TODO Auto-generated catch block
//							e.printStackTrace();
//						}
//						//FileSystemResource attc = new FileSystemResource(file);
//						//helper.addAttachment(attc.getFilename(), attc);
//					     mailer.send(message);
//				}
//		}
//		
//		Comment cmt = cmtRepository.save(comment);
//		return cmt.getId();
//	}
//	
	public List<CommentDTO> findAll() {
		List<Comment> comments = cmtRepository.findAll();
		List<CommentDTO> toReturn = new ArrayList<CommentDTO>();
		for (Comment comment : comments) {
			CommentDTO dto = new CommentDTO.Builder()
						.id(comment.getId())
						.title(comment.getTitle())
						.content(comment.getContent())
						.task(comment.getTask())
						.create();
			toReturn.add(dto);
		}
		return toReturn;
	}

	public int create(CommentDTO commentDTO) {
		Comment comment = new Comment();
		comment.setTitle(commentDTO.getTitle());
		comment.setContent(commentDTO.getContent());
		comment.setTask(commentDTO.getTask());
		
		Comment cmt = cmtRepository.save(comment);
		return cmt.getId();
	}

}
