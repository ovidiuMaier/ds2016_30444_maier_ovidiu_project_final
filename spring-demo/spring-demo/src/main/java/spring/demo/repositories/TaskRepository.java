package spring.demo.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.*;

import spring.demo.entities.Task;

public interface TaskRepository extends JpaRepository<Task, Integer> {

	//@Query("SELECT t FROM Task t LEFT JOIN FETCH t.users u WHERE t.id= :tsk")
	//Task getByIdFetchUsers(int id);
	Task findById(int id);
	
}
