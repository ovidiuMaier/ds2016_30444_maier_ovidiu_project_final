package spring.demo.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import spring.demo.entities.User;

public interface UserRepository extends JpaRepository<User, Integer> {

	User findById(int id);
	
	@Query("select u from User u where u.email = ?1")
	User findByEmail(String email);
	
	User findByUsername(String username);

}
