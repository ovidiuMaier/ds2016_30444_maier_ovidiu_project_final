package spring.demo.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.*;

import spring.demo.entities.Comment;

public interface CommentRepository extends JpaRepository<Comment, Integer> {

	//@Query("SELECT t FROM Comment t LEFT JOIN FETCH t.users u WHERE t.id= :tsk")
	//Comment getByIdFetchUsers(int id);
	Comment findById(int id);
	
}
