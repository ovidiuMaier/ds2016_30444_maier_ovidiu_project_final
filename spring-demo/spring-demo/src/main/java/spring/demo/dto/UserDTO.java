package spring.demo.dto;

import java.util.Set;

import com.fasterxml.jackson.annotation.JsonIgnore;

import spring.demo.entities.Task;

public class UserDTO {

	private Integer id;
	private String email;
	private String username;
	private String password;
	private String role;
	private Set<Task> tasks;
	
	public UserDTO() {
	}

	public UserDTO(Integer id, String email, String username, String password, String role, Set<Task> tasks) {
		super();
		this.id = id;
		this.email = email;
		this.username = username;
		this.password = password;
		this.role = role;
		this.tasks = tasks;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}
	
	public Set<Task> getTasks() {
		return tasks;
	}

	public void setTasks(Set<Task> tasks) {
		this.tasks = tasks;
	}

	public static class Builder {
		private Integer nestedid;
		private String nestedemail;
		private String nestedusername;
		private String nestedpassword;
		private String nestedrole;
		private Set<Task> nestedtasks;
	
		public Builder id(int id) {
			this.nestedid = id;
			return this;
		}

		public Builder email(String email) {
			this.nestedemail = email;
			return this;
		}
		
		public Builder username(String username) {
			this.nestedusername = username;
			return this;
		}

		public Builder password(String password) {
			this.nestedpassword = password;
			return this;
		}

		public Builder role(String role) {
			this.nestedrole = role;
			return this;
		}
		
		public Builder tasks(Set<Task> tasks) {
			this.nestedtasks = tasks;
			return this;
		}


		public UserDTO create() {
			return new UserDTO(nestedid, nestedemail, nestedusername, nestedpassword, nestedrole, nestedtasks);
		}

	}

}
