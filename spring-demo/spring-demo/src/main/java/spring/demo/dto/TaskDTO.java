package spring.demo.dto;

import java.util.Set;

import spring.demo.dto.TaskDTO.Builder;
import spring.demo.entities.*;

public class TaskDTO {

	private Integer id;
	private String title;
	private String description;
	private String status;
	private Set<User> users;
	private Set<Comment> comments;
	
	public TaskDTO() {
	}

	public TaskDTO(Integer id, String title, String description, String status, Set<User> users,Set<Comment> comments) {
		super();
		this.id = id;
		this.title = title;
		this.description = description;
		this.status = status;
		this.users = users;
		this.comments = comments;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
	public Set<User> getUsers() {
		return users;
	}

	public void setUsers(Set<User> users) {
		this.users = users;
	}

	public Set<Comment> getComments() {
		return comments;
	}

	public void setComments(Set<Comment> comments) {
		this.comments = comments;
	}

	

	public static class Builder {
		private Integer nestedid;
		private String nestedtitle;
		private String nesteddescription;
		private String nestedstatus;
		private Set<User> nestedusers;
		private Set<Comment> nestedcomments;
	
		public Builder id(int id) {
			this.nestedid = id;
			return this;
		}

		public Builder title(String title) {
			this.nestedtitle = title;
			return this;
		}

		public Builder description(String description) {
			this.nesteddescription = description;
			return this;
		}

		public Builder status(String status) {
			this.nestedstatus = status;
			return this;
		}
		
		public Builder users(Set<User> users) {
			this.nestedusers = users;
			return this;
		}
		
		public Builder comments(Set<Comment> comments) {
			this.nestedcomments = comments;
			return this;
		}
		
		public TaskDTO create() {
			return new TaskDTO(nestedid, nestedtitle, nesteddescription, nestedstatus, nestedusers, nestedcomments);
		}

	}

}
