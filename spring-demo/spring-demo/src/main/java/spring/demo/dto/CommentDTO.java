package spring.demo.dto;

import java.util.Set;

import spring.demo.dto.CommentDTO.Builder;
import spring.demo.entities.*;

public class CommentDTO {

	private Integer id;
	private String title;
	private String content;
	private Task task;
	
	public CommentDTO() {
	}

	public CommentDTO(Integer id, String title, String content, Task task) {
		super();
		this.id = id;
		this.title = title;
		this.content = content;
		this.task = task;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContent() {
		return content;
	}

	public void setCcontent(String content) {
		this.content = content;
	}

		
	public Task getTask() {
		return task;
	}

	public void setTask(Task task) {
		this.task = task;
	}


	public static class Builder {
		private Integer nestedid;
		private String nestedtitle;
		private String nestedcontent;
		private Task nestedtask;
	
		public Builder id(int id) {
			this.nestedid = id;
			return this;
		}

		public Builder title(String title) {
			this.nestedtitle = title;
			return this;
		}

		public Builder content(String content) {
			this.nestedcontent = content;
			return this;
		}

		public Builder task(Task task) {
			this.nestedtask = task;
			return this;
		}
		
		public CommentDTO create() {
			return new CommentDTO(nestedid, nestedtitle, nestedcontent, nestedtask);
		}

	}

}
