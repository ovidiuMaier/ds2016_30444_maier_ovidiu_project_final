package spring.demo.controller;

import java.util.List;

import javax.mail.MessagingException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import spring.demo.dto.TaskDTO;
import spring.demo.services.TaskService;

@CrossOrigin(maxAge = 3600)
@RestController
@RequestMapping("/task")
public class TaskController {

	@Autowired
	private TaskService taskService;

	@RequestMapping(value = "/details/{id}", method = RequestMethod.GET)
	public TaskDTO getTaskById(@PathVariable("id") int id) {
		return taskService.findTaskById(id);
	}

	@RequestMapping(value = "/all", method = RequestMethod.GET)
	public List<TaskDTO> getAllTasks() {
		return taskService.findAll();
	}

	@RequestMapping(value = "/insert", method = RequestMethod.POST)
	public int insertTask(@RequestBody TaskDTO taskDTO) {
		System.out.println(taskDTO);
		return taskService.create(taskDTO);
	}
	
	@RequestMapping(value = "/delete/{id}", method = RequestMethod.GET)
	public TaskDTO deleteTask(@PathVariable("id") int id) {
		return taskService.deleteTask(id);
	}
	
	@RequestMapping(value = "/update/{id}", method = RequestMethod.POST)
	public int updateTask(@PathVariable("id") int id, @RequestBody TaskDTO taskDTO) {
		System.out.println("controller");
		return taskService.updateTask(id, taskDTO);
	}
}

