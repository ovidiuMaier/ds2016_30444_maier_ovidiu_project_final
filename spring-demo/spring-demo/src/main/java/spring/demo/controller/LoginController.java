package spring.demo.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import spring.demo.dto.*;

import spring.demo.entities.*;
import spring.demo.services.*;

@CrossOrigin(maxAge = 3600)
@RestController
@RequestMapping("/login")
public class LoginController {
	
	@Autowired
	private UserService userService;

	@RequestMapping(value = "/perform/{email}/{password}", method = RequestMethod.POST)
	public UserDTO getLoginById(@PathVariable("email") String email, @PathVariable("password") String password) {
		System.out.println(email);
		
		//UserService service = new UserService();
	
		List<UserDTO> users = userService.findAll();
		
		UserDTO found = null;
		
		for (UserDTO user : users) {
			if (user.getEmail().equals(email) && user.getPassword().equals(password))
			{
				found =  user;
			}
		}
		return found;
	}

	
}

