package spring.demo.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.MediaType;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.*;

import spring.demo.services.*;
import spring.demo.dto.*;
import spring.demo.entities.*;

import java.util.Set;


import java.io.IOException; 
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.font.PDType1Font;

@CrossOrigin(maxAge = 3600)
@RestController
@RequestMapping("/report")
public class ReportController {

	@Autowired
	private UserService userService;

	@RequestMapping(value = "/details/{id}", method = RequestMethod.GET)
	public void getFile( @PathVariable("id") int id,
	    HttpServletResponse response, HttpServletRequest request) {
	    try {
	        PDDocument document = new PDDocument();    
	        PDPage blankPage = new PDPage();
	        document.addPage( blankPage );
	        PDPage page = document.getPage(0);
	        PDPageContentStream contentStream = new PDPageContentStream(document, page);
	        
	        //Begin the Content stream 
	        contentStream.beginText(); 
	         
	        //Setting the font to the Content stream  
	        contentStream.setFont(PDType1Font.TIMES_ROMAN, 12);

	        //Setting the position for the line 
	        contentStream.newLineAtOffset(1, 700);
	        
	        contentStream.setLeading(14.5f);

	        UserDTO userDTO = userService.findUserById(id);
	        Set<Task> tasks = userDTO.getTasks();
	        int nrTasks = tasks.size();
	        int nrCompleted = 0;
	        
	        for(Task t : tasks)
	        	if (t.getStatus().equals("completed")) nrCompleted++;
	        		        
	        String text = userDTO.getEmail();
	  
	        String text2 = "Number of tasks: " + Integer.toString(nrTasks) + "; Nr completed tasks: " + Integer.toString(nrCompleted) ;;

	        //Adding text in the form of string 
	        contentStream.showText(text);      
	        
	        contentStream.newLine();
	        
	        contentStream.showText(text2);      

	        //Ending the content stream
	        contentStream.endText();

	        String phyPath = request.getSession().getServletContext().getRealPath("/");
	        String filepath = phyPath + "WEB-INF/report" + Integer.toString(id) + ".pdf";
	        System.out.println(filepath);

	        //Closing the content stream
	        contentStream.close();



	        
	        //Saving the document
	        document.save(new File(filepath));
	        
	        System.out.println(filepath);
	        
	        //Closing the document
	        document.close();
	        
	        
	        //return new FileSystemResource(filepath); 
	        
	        File f=new File(filepath);

	       
	        InputStream is = new FileInputStream(f);
	       
	        org.apache.commons.io.IOUtils.copy(is, response.getOutputStream());
	        response.flushBuffer();
	        response.setContentType("application/pdf");

	    } catch (IOException ex) {
	   	    
	    }
	}
}

