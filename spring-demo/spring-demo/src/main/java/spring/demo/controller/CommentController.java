package spring.demo.controller;

import java.util.List;

import javax.mail.MessagingException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import spring.demo.dto.CommentDTO;
import spring.demo.services.CommentService;

@CrossOrigin(maxAge = 3600)
@RestController
@RequestMapping("/comment")
public class CommentController {

	@Autowired
	private CommentService commentService;

	@RequestMapping(value = "/details/{id}", method = RequestMethod.GET)
	public CommentDTO getCommentById(@PathVariable("id") int id) {
		return commentService.findCommentById(id);
	}

	@RequestMapping(value = "/all", method = RequestMethod.GET)
	public List<CommentDTO> getAllComments() {
		return commentService.findAll();
	}

	@RequestMapping(value = "/insert", method = RequestMethod.POST)
	public int insertComment(@RequestBody CommentDTO commentDTO) {
		System.out.println(commentDTO);
		return commentService.create(commentDTO);
	}
	
	@RequestMapping(value = "/delete/{id}", method = RequestMethod.GET)
	public CommentDTO deleteComment(@PathVariable("id") int id) {
		return commentService.deleteComment(id);
	}
	
	/*
	
	@RequestMapping(value = "/update/{id}", method = RequestMethod.POST)
	public int updateComment(@PathVariable("id") int id, @RequestBody CommentDTO commentDTO) {
		System.out.println("controller");
		return commentService.updateComment(id, commentDTO);
	}
	*/
}

