package spring.demo.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import spring.demo.dto.TaskDTO;
import spring.demo.dto.UserDTO;
import spring.demo.services.UserService;

@CrossOrigin(maxAge = 3600)
@RestController
@RequestMapping("/user")
public class UserController {

	@Autowired
	private UserService userService;

	@RequestMapping(value = "/details/{id}", method = RequestMethod.GET)
	public UserDTO getUserById(@PathVariable("id") int id) {
		return userService.findUserById(id);
	}
	
	@RequestMapping(value = "/username/{username}", method = RequestMethod.GET)
	public UserDTO getUserByUsername(@PathVariable("username") String username) {
		System.out.println(username);
		return userService.findUserByUsername(username);
	}


	@RequestMapping(value = "/all", method = RequestMethod.GET)
	public List<UserDTO> getAllUsers() {
		return userService.findAll();
	}
	
	@RequestMapping(value = "/all/clients", method = RequestMethod.GET)
	public List<UserDTO> getAllClients() {
		return userService.findAllClients();
	}
	
	@RequestMapping(value = "/all/members", method = RequestMethod.GET)
	public List<UserDTO> getAllMemebers() {
		return userService.findAllMembers();
	}

	@RequestMapping(value = "/insert", method = RequestMethod.POST)
	public int insertUser(@RequestBody UserDTO userDTO) {
		return userService.create(userDTO);
	}
	
	@RequestMapping(value = "/delete/{id}", method = RequestMethod.GET)
	public UserDTO deleteUser(@PathVariable("id") int id) {
		return userService.deleteUser(id);
	}
	
	@RequestMapping(value = "/update/{id}", method = RequestMethod.POST, consumes="application/json")
	public int updateUser(HttpServletRequest request, @PathVariable("id") int id, @RequestBody UserDTO userDTO) {
		return userService.updateUser(id, userDTO);
//		System.out.println(userDTO);
		
	}

}

