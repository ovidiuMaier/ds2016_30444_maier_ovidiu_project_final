(function() {
	var commentServices = angular.module('commentServices', []);

	commentServices.factory('CommentFactory', [ '$http', 'config',
			function($http, config) {

				var privateCommentDetails = function(id) {
					return $http.get(config.API_URL + '/comment/details/' + id);
				};
				
				var privateCommentCommentname = function(commentname) {
					
					return $http.get(config.API_URL + '/comment/commentname/' + commentname);
				};

				var privateCommentList = function() {
					return $http.get(config.API_URL + '/comment/all');
				};
				
				var privateClientList = function() {
					return $http.get(config.API_URL + '/comment/all/clients');
				};
				
				var privateMemberList = function() {
					return $http.get(config.API_URL + '/comment/all/members');
				};
				
				var privateCreateComment = function(title, content, taskid, tasktitle, taskdescription, taskstatus) {
					data = { title: title, content: content, task: {id: taskid, title: tasktitle, description: taskdescription, status: taskstatus}};
					//alert(taskid);
					return $http.post(config.API_URL + '/comment/insert', data);
				};
				
				var privateUpdateComment = function(id, commentname, password, email, role) {
					data = { commentname: commentname, email: email, password: password, role: role};
					return $http.post(config.API_URL + '/comment/update/' + id, data);
				};
				
				var privateDeleteComment = function(id) {
					return $http.get(config.API_URL + '/comment/delete/' + id);
				};
				
					 

				return {
					findById : function(id) {
						return privateCommentDetails(id);
					},
					
					findByCommentname : function(commentname) {
						
						return privateCommentCommentname(commentname);
					},

					findAll : function() {
						return privateCommentList();
					},
					
					findAllClients : function() {
						return privateClientList();
					},
					
					findAllMembers : function() {
						return privateMemberList();
					},
					
					create : function(title, content, taskid, tasktitle, taskdescription, taskstatus){
						//alert(taskid);
						return privateCreateComment(title, content, taskid, tasktitle, taskdescription, taskstatus);
					},
					
					update : function(id, commentname, password, email, role){
						return privateUpdateComment(id, commentname, password, email, role);
					},
					
					remove : function(id){
						return privateDeleteComment(id);
					}
					
					
				};
			} ]);

})();