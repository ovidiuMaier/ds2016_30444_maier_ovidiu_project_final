(function () {
    'use strict';
 
    angular
        .module('angularjs-demo')
        .factory('AuthenticationService', AuthenticationService);
 
    AuthenticationService.$inject = ['$http', '$cookies', '$rootScope', '$timeout', 'UserFactory'];
    function AuthenticationService($http, $cookies, $rootScope, $timeout, UserFactory) {
        var service = {};
 
        service.Login = Login;
        service.SetCredentials = SetCredentials;
        service.ClearCredentials = ClearCredentials;
 
        return service;
 
        function Login(username, password, callback) {
 
            /* Dummy authentication for testing, uses $timeout to simulate api call
             ----------------------------------------------*/

        	/*
        	//            $timeout(function () {
                var response;
                UserFactory.findByUsername(username)
                    .then(function (user) {
                        if (user !== null && user.password === password) {
                            response = { success: true };
                        } else {
                            response = { success: false, message: 'Username or password is incorrect' };
                       }
                        callback(response);
                    });
//            });
 /*
            /* Use this for real authentication
             ----------------------------------------------*/
        /*    
        	var response;
        	var call = $http.post(config.API_URL + '/login', { username: username, password: password });
            call.success({
            	response = { success: true };
			}).error(function( {
				response = { success: false, message: 'Username or password is incorrect' };
			});

			callback(response);
          */  
           
        	//var success  = { success: true };
        	var fail = { success: false, message: 'Username or password is incorrect' };
        	var user;
        	$http.get('http://localhost:8080/spring-demo' + '/user/username/' + username)
        	//$http.get('http://localhost:8080/spring-demo' + '/user/details/10')
            .success(function(response) {
            	var promise = UserFactory.findByUsername(username);
				promise.success(function(data) {
					user = data;
					alert(user.role);
					var success = {success: true, role: user.role }
	            	callback(success);
	
				});
					
//            	var success = {success: true, role: user.role }
  //          	callback(success);

            }).error(function() {
            	callback(fail);
            });
            
        	/*
        	var response;
            UserFactory.findByUsername(username)
                .then(function (user) {
                    if (user !== null && user.password === password) {
                        response = { success: true, role: user.role };
                    } else {
                        response = { success: false, message: 'Username or password is incorrect' };
                   }
                    callback(response);
                });
        	*/
        	   
 
        }
 
        function SetCredentials(username, password, role) {
            var authdata = Base64.encode(username + ':' + password);
 
            $rootScope.globals = {
                currentUser: {
                    username: username,
                    role: role,
                    authdata: authdata
                }
            };
            
            //accesezi cu $rootScope.globals.currentUser.username
            //pasezi $rootScope unde ai nevoie 
            //$location.path('#/something')
            //pt pdf window.open("local/reports/" + id, 'Report', 'width=500, height=400');
            //window.location.hreg("#/students");
            
 
            // set default auth header for http requests
            $http.defaults.headers.common['Authorization'] = 'Basic ' + authdata;
 
            // store user details in globals cookie that keeps user logged in for 1 week (or until they logout)
            var cookieExp = new Date();
            cookieExp.setDate(cookieExp.getDate() + 7);
            $cookies.putObject('globals', $rootScope.globals, { expires: cookieExp });
        }
 
        function ClearCredentials() {
            $rootScope.globals = {};
            $cookies.remove('globals');
            $http.defaults.headers.common.Authorization = 'Basic';
        }
    }
 
    // Base64 encoding service used by AuthenticationService
    var Base64 = {
 
        keyStr: 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=',
 
        encode: function (input) {
            var output = "";
            var chr1, chr2, chr3 = "";
            var enc1, enc2, enc3, enc4 = "";
            var i = 0;
 
            do {
                chr1 = input.charCodeAt(i++);
                chr2 = input.charCodeAt(i++);
                chr3 = input.charCodeAt(i++);
 
                enc1 = chr1 >> 2;
                enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
                enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
                enc4 = chr3 & 63;
 
                if (isNaN(chr2)) {
                    enc3 = enc4 = 64;
                } else if (isNaN(chr3)) {
                    enc4 = 64;
                }
 
                output = output +
                    this.keyStr.charAt(enc1) +
                    this.keyStr.charAt(enc2) +
                    this.keyStr.charAt(enc3) +
                    this.keyStr.charAt(enc4);
                chr1 = chr2 = chr3 = "";
                enc1 = enc2 = enc3 = enc4 = "";
            } while (i < input.length);
 
            return output;
        },
 
        decode: function (input) {
            var output = "";
            var chr1, chr2, chr3 = "";
            var enc1, enc2, enc3, enc4 = "";
            var i = 0;
 
            // remove all characters that are not A-Z, a-z, 0-9, +, /, or =
            var base64test = /[^A-Za-z0-9\+\/\=]/g;
            if (base64test.exec(input)) {
                window.alert("There were invalid base64 characters in the input text.\n" +
                    "Valid base64 characters are A-Z, a-z, 0-9, '+', '/',and '='\n" +
                    "Expect errors in decoding.");
            }
            input = input.replace(/[^A-Za-z0-9\+\/\=]/g, "");
 
            do {
                enc1 = this.keyStr.indexOf(input.charAt(i++));
                enc2 = this.keyStr.indexOf(input.charAt(i++));
                enc3 = this.keyStr.indexOf(input.charAt(i++));
                enc4 = this.keyStr.indexOf(input.charAt(i++));
 
                chr1 = (enc1 << 2) | (enc2 >> 4);
                chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
                chr3 = ((enc3 & 3) << 6) | enc4;
 
                output = output + String.fromCharCode(chr1);
 
                if (enc3 != 64) {
                    output = output + String.fromCharCode(chr2);
                }
                if (enc4 != 64) {
                    output = output + String.fromCharCode(chr3);
                }
 
                chr1 = chr2 = chr3 = "";
                enc1 = enc2 = enc3 = enc4 = "";
 
            } while (i < input.length);
 
            return output;
        }
    };
 
})();