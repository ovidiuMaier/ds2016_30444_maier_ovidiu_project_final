(function() {
	var taskServices = angular.module('taskServices', []);

	taskServices.factory('TaskFactory', [ 'UserFactory', '$http', 'config', 
			function(UserFactory, $http, config) {

				var privateTaskDetails = function(id) {
					return $http.get(config.API_URL + '/task/details/' + id);
				};
				
				var privateTaskTaskname = function(taskname) {
					return $http.get(config.API_URL + '/task/taskname/' + taskname);
				};

				var privateTaskList = function() {
					return $http.get(config.API_URL + '/task/all');
				};
				
				var privateCreateTask = function(title, description, status, id, email, username, role, id2,email2,username2,role2) {
					data = { description: description, title: title, status: status, users: [{id: id, email: email, username: username, role: role}, {id: id2, email: email2, username: username2, role: role2}]};					
					return $http.post(config.API_URL + '/task/insert', data);
				};
				
				var privateUpdateTask = function(id, title, description, status) {
					data = { description: description, title: title, status: status};
					return $http.post(config.API_URL + '/task/update/' + id, data);
				};
				
				var privateDeleteTask = function(id) {
					return $http.get(config.API_URL + '/task/delete/' + id);
				};
				
					 

				return {
					findById : function(id) {
						return privateTaskDetails(id);
					},
					
					findByTaskname : function(taskname) {
						return privateTaskTaskname(taskname);
					},

					findAll : function() {
						return privateTaskList();
					},
					
					create : function(title, description, status, id, email, username, role, id2,email2,username2,role2){
						//alert(user_data);
						return privateCreateTask(title, description, status, id, email, username, role, id2,email2,username2,role2);
					},
					
					update : function(id, title, description, status){
						return privateUpdateTask(id, title, description, status);
					},
					
					remove : function(id){
						return privateDeleteTask(id);
					}
					
					
				};
			} ]);

})();