(function() {
	var userServices = angular.module('userServices', []);

	userServices.factory('UserFactory', [ '$http', 'config',
			function($http, config) {

				var privateUserDetails = function(id) {
					return $http.get(config.API_URL + '/user/details/' + id);
				};
				
				var privateUserUsername = function(username) {
					
					return $http.get(config.API_URL + '/user/username/' + username);
				};

				var privateUserList = function() {
					return $http.get(config.API_URL + '/user/all');
				};
				
				var privateClientList = function() {
					return $http.get(config.API_URL + '/user/all/clients');
				};
				
				var privateMemberList = function() {
					return $http.get(config.API_URL + '/user/all/members');
				};
				
				var privateCreateUser = function(username, password, email, role) {
					data = { username: username, email: email, password: password, role: role};
					return $http.post(config.API_URL + '/user/insert', data);
				};
				
				var privateUpdateUser = function(id, username, password, email, role) {
					data = { username: username, email: email, password: password, role: role};
					return $http.post(config.API_URL + '/user/update/' + id, data);
				};
				
				var privateDeleteUser = function(id) {
					return $http.get(config.API_URL + '/user/delete/' + id);
				};
				
					 

				return {
					findById : function(id) {
						return privateUserDetails(id);
					},
					
					findByUsername : function(username) {
						
						return privateUserUsername(username);
					},

					findAll : function() {
						return privateUserList();
					},
					
					findAllClients : function() {
						return privateClientList();
					},
					
					findAllMembers : function() {
						return privateMemberList();
					},
					
					create : function(username, password, email, role){
						return privateCreateUser(username, password, email, role);
					},
					
					update : function(id, username, password, email, role){
						return privateUpdateUser(id, username, password, email, role);
					},
					
					remove : function(id){
						return privateDeleteUser(id);
					}
					
					
				};
			} ]);

})();