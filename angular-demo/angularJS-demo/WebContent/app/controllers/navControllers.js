(function() {

	var homeModule = angular.module('navControllers', [ 'ngRoute' ])

	homeModule.config(function($routeProvider) {
		$routeProvider.when('/', {
			templateUrl : 'app/views/homeview.html',
			controller : 'HomeController',
			controllerAs : "homeCtrl"
		})
		
		.when('/deleted', {
			templateUrl : 'app/views/user/deleted.html',
			controller : 'del',
			controllerAs : "del"
		})
		
//		 .when('/login', {
//			 	templateUrl: 'app/views/loginview.html',
//			 	controller: 'LoginController',
//                controllerAs: 'vm'
//         })
//         
//         .otherwise({ redirectTo: '/login' })
            
	});

	homeModule.controller('HomeController', function($scope) {
		$scope.message = "Message from the controller...";
	});

})();