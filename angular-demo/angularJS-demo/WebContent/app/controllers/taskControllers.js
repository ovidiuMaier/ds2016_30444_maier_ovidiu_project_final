(function() {

	var tasksModule = angular.module('taskControllers', [ 'ngRoute' ])

	tasksModule.config(function($routeProvider) {
		$routeProvider.when('/tasks', {
			templateUrl : 'app/views/task/task-list.html',
			controller : 'AllTasksController',
			controllerAs : "allTasksCtrl"
		}).when('/tasksregular', {
			templateUrl : 'app/views/task/task-list-regular.html',
			controller : 'AllRegularController',
			controllerAs : "regCtrl"
		}).when('/task/:id', {
			templateUrl : 'app/views/task/task-details.html',
			controller : 'TaskController',
			controllerAs : "taskCtrl"
		})
		.when('/tasks/new', {
			templateUrl : 'app/views/task/new-task.html',
			controller : 'NewTaskController',
			controllerAs : "vm"
		})
		.when('/task/report/:id', {
			templateUrl : 'app/views/task/task-list.html',
			controller : 'ReportTaskController',
			controllerAs : "vm"
		})
		
		.when('/task/delete/:id', {
			templateUrl : 'app/views/task/task-list.html',
			controller : 'DeleteTaskController',
			controllerAs : "vm"
		})
		
		.when('/task/edit/:id', {
			templateUrl : 'app/views/task/edit-task.html',
			controller : 'EditTaskController',
			controllerAs : "vm"
		})
		
		

	});

	tasksModule.controller('AllTasksController', [ '$scope', 'TaskFactory',
			function($scope, TaskFactory) {
				$scope.tasks = [];
				var promise = TaskFactory.findAll();
				promise.success(function(data) {
					$scope.tasks = data;
				}).error(function(data, status, header, config) {
					//alert(status);
				});

			} ]);
	
	tasksModule.controller('AllRegularController', [ '$scope', 'TaskFactory',
		function($scope, TaskFactory) {
			$scope.tasks = [];
			var promise = TaskFactory.findAll();
			promise.success(function(data) {
				$scope.tasks = data;
			}).error(function(data, status, header, config) {
				//alert(status);
			});

		} ]);


	tasksModule.controller('TaskController', [ '$scope', '$routeParams',
			'TaskFactory', function($scope, $routeParams, TaskFactory) {
				var id = $routeParams.id;
				var promise = TaskFactory.findById(id);
				$scope.task = null;
				promise.success(function(data) {
					$scope.task = data;
				}).error(function(data, status, header, config) {
					//alert(status);
				});
			} ]);
	
	/*
	tasksModule.controller('NewTaskController', [ '$scope', '$routeParams',
		'TaskFactory', function($scope, $routeParams, TaskFactory) {
		var vm=this;
		vm.create = create;
		
		function create(){
			vm.dataLoading = true;
			alert(vm.taskname);
			Task.factory.create(vm.taskname, vm.password);
		};
		 
		} ]);
	*/
	tasksModule.controller('NewTaskController', NewTaskController);
	
	NewTaskController.$inject = ['$http', '$scope', '$window', 'AuthenticationService', 'FlashService', '$rootScope', 'TaskFactory', 'UserFactory'];
    function NewTaskController($http, $scope, $window, AuthenticationService, FlashService, $rootScope, TaskFactory, UserFactory) {
        var vm = this;
        var promise = UserFactory.findAllMembers();
		promise.success(function(data) {
			$scope.availableOptions = data;
		});
		
		var promise2 = UserFactory.findAllClients();
		promise2.success(function(data) {
			$scope.availableClients = data;
		});
        
		//$scope.selectedOption = null;
        vm.create = create;
 
        function create() {
        	vm.dataLoading = true;
			//alert(vm.password);
        	var user;
        	//alert(vm.selectedOption);
        	/*
        	var promise3 = UserFactory.findByUsername(vm.selectedClient);
			promise3.success(function(data){
				#scope.role = data.role;
				alert(data);
			});*/
        	
        	var prom = UserFactory.findByUsername(vm.selectedClient);
        	prom.success(function(data){
        		$scope.id = data.id;
        		$scope.username = data.username;
        		$scope.email = data.email;
        		$scope.password = data.password;
        		$scope.role = data.role;
        	});
			
        	var role;
        	$http.get('http://localhost:8080/spring-demo' + '/user/username/' + vm.selectedOption)
            .success(function(response) {
            	var promise = UserFactory.findByUsername(vm.selectedOption);
				promise.success(function(data) {
					user = data;
					//alert(vm.title);
					role = user.role;
					//alert($scope.role);
				//	var client = UserFactory.findByUsername(vm.selectedClient);
				//	alert(client.data);
					//var user_data = {id: user.id, email: user.email, username: user.username, role: user.role };
					TaskFactory.create(vm.title,vm.description, vm.status, user.id, user.email, user.username, user.role,$scope.id,$scope.email,$scope.username,$scope.role );
					$window.location.href = "#/tasks";
					$window.location.reload();
				});
            });
			
        	
			//var user_data = {email: user.email, username: user.username, role: user.role }
			//TaskFactory.create(vm.title,vm.description, vm.status, user_data);
			//$window.location.href = "#/tasks";
			//$window.location.reload();
		};
    }
    
    tasksModule.controller('DeleteTaskController', DeleteTaskController);
	
	DeleteTaskController.$inject = ['$scope', '$window', 'AuthenticationService', 'FlashService', '$rootScope', 'TaskFactory', '$routeParams'];
    function DeleteTaskController($scope, $window, AuthenticationService, FlashService, $rootScope, TaskFactory, $routeParams) {	
        	var id = $routeParams.id;
        	//alert(id);
			TaskFactory.remove(id);
			$scope.tasks = [];
			var promise = TaskFactory.findAll();
			promise.success(function(data) {
				$scope.tasks = data;
			});
			//$location.path('/tasks');
			//$location.path("/tasks");
			$window.location.href = "#/tasks";
			$window.location.reload();
		
    }
    
    tasksModule.controller('ReportTaskController', ReportTaskController);
	
	ReportTaskController.$inject = ['$scope', '$window', 'AuthenticationService', 'FlashService', '$rootScope', 'TaskFactory', '$routeParams'];
    function ReportTaskController($scope, $window, AuthenticationService, FlashService, $rootScope, TaskFactory, $routeParams) {	
        	var id = $routeParams.id;
        	//alert(id);
        	 window.location = "http://localhost:8080/spring-demo/report/details/" + id;
			
    }
    
    tasksModule.controller('EditTaskController', EditTaskController);
	
	EditTaskController.$inject = ['$window', 'AuthenticationService', 'FlashService', '$rootScope', 'TaskFactory', '$routeParams'];
    function EditTaskController($window, AuthenticationService, FlashService, $rootScope, TaskFactory, $routeParams) {
        var vm = this;
        var id = $routeParams.id;
		var promise = TaskFactory.findById(id);
		
		promise.success(function(data) {
			vm.title = data.title;
			vm.description = data.description;
			vm.status = data.status;
		});
 
        vm.update = update;
 
        function update() {
        	vm.dataLoading = true;
			//alert(vm.password);
			TaskFactory.update(id, vm.title,vm.description, vm.status);
			$window.location.href = "#/tasks";
			$window.location.reload();
		};
    }



	var gems = [ {
		id : 1,
		name : 'My name1',
		address : "address 1",
		occupation : 'Something1'
	}, {
		id : 2,
		name : 'My name2',
		address : "address 2",
		occupation : 'Something2'
	} ];

})();
