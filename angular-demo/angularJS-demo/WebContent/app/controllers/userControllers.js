(function() {

	var usersModule = angular.module('userControllers', [ 'ngRoute' ])

	usersModule.config(function($routeProvider) {
		$routeProvider.when('/users', {
			templateUrl : 'app/views/user/user-list.html',
			controller : 'AllUsersController',
			controllerAs : "allUsersCtrl"
		}).when('/user/:id', {
			templateUrl : 'app/views/user/user-details.html',
			controller : 'UserController',
			controllerAs : "userCtrl"
		})
		.when('/users/new', {
			templateUrl : 'app/views/user/new-user.html',
			controller : 'NewUserController',
			controllerAs : "vm"
		})
		.when('/user/report/:id', {
			templateUrl : 'app/views/user/user-list.html',
			controller : 'ReportUserController',
			controllerAs : "vm"
		})
		
		.when('/user/delete/:id', {
			templateUrl : 'app/views/user/user-list.html',
			controller : 'DeleteUserController',
			controllerAs : "vm"
		})
		
		.when('/user/edit/:id', {
			templateUrl : 'app/views/user/edit-user.html',
			controller : 'EditUserController',
			controllerAs : "vm"
		})
		
		

	});

	usersModule.controller('AllUsersController', [ '$scope', 'UserFactory',
			function($scope, UserFactory) {
				$scope.users = [];
				var promise = UserFactory.findAll();
				promise.success(function(data) {
					$scope.users = data;
				}).error(function(data, status, header, config) {
					//alert(status);
				});

			} ]);

	usersModule.controller('UserController', [ '$scope', '$routeParams',
			'UserFactory', function($scope, $routeParams, UserFactory) {
				var id = $routeParams.id;
				var promise = UserFactory.findById(id);
				$scope.user = null;
				promise.success(function(data) {
					$scope.user = data;
				}).error(function(data, status, header, config) {
					//alert(status);
				});
			} ]);
	
	/*
	usersModule.controller('NewUserController', [ '$scope', '$routeParams',
		'UserFactory', function($scope, $routeParams, UserFactory) {
		var vm=this;
		vm.create = create;
		
		function create(){
			vm.dataLoading = true;
			alert(vm.username);
			User.factory.create(vm.username, vm.password);
		};
		 
		} ]);
	*/
	usersModule.controller('NewUserController', NewUserController);
	
	NewUserController.$inject = ['$window', 'AuthenticationService', 'FlashService', '$rootScope', 'UserFactory'];
    function NewUserController($window, AuthenticationService, FlashService, $rootScope, UserFactory) {
        var vm = this;
 
        vm.create = create;
 
        function create() {
        	vm.dataLoading = true;
			//alert(vm.password);
			UserFactory.create(vm.username, vm.password,vm.email, vm.role);
			$window.location.href = "#/users";
			$window.location.reload();
		};
    }
    
    usersModule.controller('DeleteUserController', DeleteUserController);
	
	DeleteUserController.$inject = ['$scope', '$window', 'AuthenticationService', 'FlashService', '$rootScope', 'UserFactory', '$routeParams'];
    function DeleteUserController($scope, $window, AuthenticationService, FlashService, $rootScope, UserFactory, $routeParams) {	
        	var id = $routeParams.id;
        	//alert(id);
			UserFactory.remove(id);
			$scope.users = [];
			var promise = UserFactory.findAll();
			promise.success(function(data) {
				$scope.users = data;
			});
			//$location.path('/users');
			//$location.path("/users");
			$window.location.href = "#/users";
			$window.location.reload();
		
    }
    
    usersModule.controller('ReportUserController', ReportUserController);
	
	ReportUserController.$inject = ['$scope', '$window', 'AuthenticationService', 'FlashService', '$rootScope', 'UserFactory', '$routeParams'];
    function ReportUserController($scope, $window, AuthenticationService, FlashService, $rootScope, UserFactory, $routeParams) {	
        	var id = $routeParams.id;
        	//alert(id);
        	 window.location = "http://localhost:8080/spring-demo/report/details/" + id;
			
    }
    
    usersModule.controller('EditUserController', EditUserController);
	
	EditUserController.$inject = ['$window', 'AuthenticationService', 'FlashService', '$rootScope', 'UserFactory', '$routeParams'];
    function EditUserController($window, AuthenticationService, FlashService, $rootScope, UserFactory, $routeParams) {
        var vm = this;
        var id = $routeParams.id;
		var promise = UserFactory.findById(id);
		
		promise.success(function(data) {
			vm.password = data.password;
			vm.email = data.email;
			vm.role = data.role;
			vm.username = data.username;
		});
 
        vm.update = update;
 
        function update() {
        	vm.dataLoading = true;
			//alert(vm.password);
			UserFactory.update(id,vm.username, vm.password,vm.email, vm.role);
			$window.location.href = "#/users";
			$window.location.reload();
		};
    }



	var gems = [ {
		id : 1,
		name : 'My name1',
		address : "address 1",
		occupation : 'Something1'
	}, {
		id : 2,
		name : 'My name2',
		address : "address 2",
		occupation : 'Something2'
	} ];

})();
