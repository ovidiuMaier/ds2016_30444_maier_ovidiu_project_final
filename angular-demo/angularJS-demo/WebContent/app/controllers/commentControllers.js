(function() {

	var commentsModule = angular.module('commentControllers', [ 'ngRoute' ])

	commentsModule.config(function($routeProvider) {
		$routeProvider.when('/comments', {
			templateUrl : 'app/views/comment/comment-list.html',
			controller : 'AllCommentsController',
			controllerAs : "allCommentsCtrl"
		}).when('/comment/:id', {
			templateUrl : 'app/views/comment/comment-details.html',
			controller : 'CommentController',
			controllerAs : "commentCtrl"
		})
		.when('/comments/new/:id', {
			templateUrl : 'app/views/comment/new-comment.html',
			controller : 'NewCommentController',
			controllerAs : "vm"
		})
		.when('/comment/report/:id', {
			templateUrl : 'app/views/comment/comment-list.html',
			controller : 'ReportCommentController',
			controllerAs : "vm"
		})
		
		.when('/comment/delete/:id', {
			templateUrl : 'app/views/comment/comment-list.html',
			controller : 'DeleteCommentController',
			controllerAs : "vm"
		})
		
		.when('/comment/edit/:id', {
			templateUrl : 'app/views/comment/edit-comment.html',
			controller : 'EditCommentController',
			controllerAs : "vm"
		})
		
		

	});

	commentsModule.controller('AllCommentsController', [ '$scope', 'CommentFactory',
			function($scope, CommentFactory) {
				$scope.comments = [];
				var promise = CommentFactory.findAll();
				promise.success(function(data) {
					$scope.comments = data;
				}).error(function(data, status, header, config) {
					//alert(status);
				});

			} ]);

	commentsModule.controller('CommentController', [ '$scope', '$routeParams',
			'CommentFactory', function($scope, $routeParams, CommentFactory) {
				var id = $routeParams.id;
				var promise = CommentFactory.findById(id);
				$scope.comment = null;
				promise.success(function(data) {
					$scope.comment = data;
				}).error(function(data, status, header, config) {
					//alert(status);
				});
			} ]);
	
	/*
	commentsModule.controller('NewCommentController', [ '$scope', '$routeParams',
		'CommentFactory', function($scope, $routeParams, CommentFactory) {
		var vm=this;
		vm.create = create;
		
		function create(){
			vm.dataLoading = true;
			alert(vm.commentname);
			Comment.factory.create(vm.commentname, vm.password);
		};
		 
		} ]);
	*/
	commentsModule.controller('NewCommentController', NewCommentController);
	
	NewCommentController.$inject = ['$scope', '$window', 'AuthenticationService', 'FlashService', '$rootScope', 'CommentFactory', '$routeParams', 'TaskFactory'];
    function NewCommentController($scope, $window, AuthenticationService, FlashService, $rootScope, CommentFactory, $routeParams, TaskFactory) {
    	
    	var id = $routeParams.id;
    	
    	var prom = TaskFactory.findById(id);
    	prom.success(function(data){
    		$scope.id = data.id;
    		$scope.title = data.title;
    		$scope.description = data.description;
    		$scope.status = data.status;
    	});
    	
    	
        var vm = this;
 
        vm.create = create;
 
        function create() {
        	
        	vm.dataLoading = true;
			//alert(vm.password);
        	var id = $scope.id;
        	
        	var ttitle = $scope.title;
        	var desc = $scope.description;
        	var status = $scope.status;
			//CommentFactory.create(vm.title, vm.content, $scope,id, $scope.title, $scope.description, $scope.status);
			CommentFactory.create(vm.title, vm.content, id, ttitle, desc, status);
			$window.location.href = "#/tasksregular";
			$window.location.reload();
		};
    }
    
    commentsModule.controller('DeleteCommentController', DeleteCommentController);
	
	DeleteCommentController.$inject = ['$scope', '$window', 'AuthenticationService', 'FlashService', '$rootScope', 'CommentFactory', '$routeParams'];
    function DeleteCommentController($scope, $window, AuthenticationService, FlashService, $rootScope, CommentFactory, $routeParams) {	
        	var id = $routeParams.id;
        	//alert(id);
			CommentFactory.remove(id);
			$scope.comments = [];
			var promise = CommentFactory.findAll();
			promise.success(function(data) {
				$scope.comments = data;
			});
			//$location.path('/comments');
			//$location.path("/comments");
			$window.location.href = "#/comments";
			$window.location.reload();
		
    }
    
    commentsModule.controller('ReportCommentController', ReportCommentController);
	
	ReportCommentController.$inject = ['$scope', '$window', 'AuthenticationService', 'FlashService', '$rootScope', 'CommentFactory', '$routeParams'];
    function ReportCommentController($scope, $window, AuthenticationService, FlashService, $rootScope, CommentFactory, $routeParams) {	
        	var id = $routeParams.id;
        	//alert(id);
        	 window.location = "http://localhost:8080/spring-demo/report/details/" + id;
			
    }
    
    commentsModule.controller('EditCommentController', EditCommentController);
	
	EditCommentController.$inject = ['$window', 'AuthenticationService', 'FlashService', '$rootScope', 'CommentFactory', '$routeParams'];
    function EditCommentController($window, AuthenticationService, FlashService, $rootScope, CommentFactory, $routeParams) {
        var vm = this;
        var id = $routeParams.id;
		var promise = CommentFactory.findById(id);
		
		promise.success(function(data) {
			vm.password = data.password;
			vm.email = data.email;
			vm.role = data.role;
			vm.commentname = data.commentname;
		});
 
        vm.update = update;
 
        function update() {
        	vm.dataLoading = true;
			//alert(vm.password);
			CommentFactory.update(id,vm.commentname, vm.password,vm.email, vm.role);
			$window.location.href = "#/comments";
			$window.location.reload();
		};
    }



	var gems = [ {
		id : 1,
		name : 'My name1',
		address : "address 1",
		occupation : 'Something1'
	}, {
		id : 2,
		name : 'My name2',
		address : "address 2",
		occupation : 'Something2'
	} ];

})();
