(function () {
    'use strict';
    
    angular
    	.module('angularjs-demo')
    	.controller('LoginController', LoginController);
  
   // var loginModule = angular.module('LoginController', [ 'ngRoute' ])
 
    LoginController.$inject = ['$location', 'AuthenticationService', 'FlashService', '$rootScope'];
    function LoginController($location, AuthenticationService, FlashService, $rootScope) {
        var vm = this;
 
        vm.login = login;
 
        (function initController() {
            // reset login status
            AuthenticationService.ClearCredentials();
        })();
 
        function login() {
            vm.dataLoading = true;
            AuthenticationService.Login(vm.username, vm.password, function (response) {
                if (response.success) {
                    AuthenticationService.SetCredentials(vm.username, vm.password, response.role);
                    //alert($rootScope.globals.currentUser.username);
                    if ($rootScope.globals.currentUser.role == "client"){
                    	$location.path('/tasksregular');
                    }
                    else if ($rootScope.globals.currentUser.role == "manager"){
                    	$location.path('/users');
                    }
                    else if ($rootScope.globals.currentUser.role == "member"){
                    	$location.path('/tasksregular');
                    }
        	
                    
                } else {
//                	alert("aici");
                    FlashService.Error(response.message);
                    vm.dataLoading = false;
                }
            });
        };
    }
 
})();